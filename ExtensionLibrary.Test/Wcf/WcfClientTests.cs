﻿using ExtensionLibrary.Test.Stub.WcfService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace ExtensionLibrary.Test.Wcf
{
    [TestClass]
    public class WcfClientTests
    {
        [TestMethod]
        public void CallWithException_InUsingBlock()
        {
            var response = new CompositeType();
            using (var client = new Service1Client())
            {
                try
                {
                    response = client.GetDataUsingDataContract(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.FullMessage());
                }
            }
        }

        [TestMethod]
        public void CallWithException()
        {
            var response = new CompositeType();
            using (var client = new Service1Client())
            {
                try
                {
                    response = client.GetDataUsingDataContract(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.FullMessage());
                }
                finally
                {
                    ((IDisposable)client).Dispose();
                }
            }
        }

        [TestMethod]
        public void CallWithDisposeSafely()
        {
            var response = new CompositeType();
            using (var client = new Service1Client())
            {
                try
                {
                    response = client.GetDataUsingDataContract(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.FullMessage());
                }
                finally
                {
                    client.DisposeSafely();
                }
            }
        }
    }
}