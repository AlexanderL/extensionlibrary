﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ExtensionLibrary.Test.Core
{
    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }
    }

    internal class UserModel
    {
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public int Age { get; set; }
    }

    [TestClass]
    public class MapTests
    {
        [TestMethod]
        public void MapWithStringRefliction()
        {
            int year = 1986;
            var source = new User
            {
                FirstName = "Alexander",
                LastName = "Lesnyakov",
                BirthDate = new DateTime(year, 1, 1)
            };

            var map = new Map<User, UserModel>(source);
            map.Populate("DateOfBirth", "BirthDate")
                .Populate("Name", s => s.FirstName + " " + s.LastName)
                .Populate("Age", s => (int)DateTime.Today.Subtract(s.BirthDate).TotalDays / 365);

            var target = map.Target;
            Assert.AreEqual("Alexander Lesnyakov", target.Name);
            Assert.AreEqual(source.BirthDate, target.DateOfBirth);
            Assert.AreEqual(DateTime.Now.Year - year, target.Age);
        }

        [TestMethod]
        public void MapWithExpressionRefliction()
        {
            int yearOfBirth = 1986;
            var source = new User
            {
                FirstName = "Alexander",
                LastName = "Lesnyakov",
                BirthDate = new DateTime(yearOfBirth, 1, 1)
            };

            var map = new Map<User, UserModel>(source);
            map.Populate(t => t.DateOfBirth, s => s.BirthDate)
                .Populate(t => t.Name, s => s.FirstName + " " + s.LastName)
                //not a very useful code
                .Populate(t => t.Age, s => (int)DateTime.Today.Subtract(s.BirthDate).TotalDays / 365);

            var target = map.Target;
            Assert.AreEqual("Alexander Lesnyakov", target.Name);
            Assert.AreEqual(source.BirthDate, target.DateOfBirth);
            Assert.AreEqual(DateTime.Today.Year - yearOfBirth, target.Age);
        }
    }
}