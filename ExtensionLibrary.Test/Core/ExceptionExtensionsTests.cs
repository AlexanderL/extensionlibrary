﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace ExtensionLibrary.Test.Core
{
    [TestClass]
    public class ExceptionExtensionsTests
    {
        [TestMethod]
        public void DevidedBy1()
        {
            Assert.AreEqual(10, Devide(10, 1));
        }

        [TestMethod]
        public void DevidedBy0()
        {
            try
            {
                Devide(10, 0);
                Assert.Fail("Should throw exception");
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.Message);
                //Debug.WriteLine(ex);
                Debug.WriteLine(ex.FullMessage());
                Assert.IsInstanceOfType(ex, typeof(ApplicationException));
            }
        }

        private object Devide(int amount, int by)
        {
            try
            {
                return amount / by;
            }
            catch (Exception ex)
            {
                var invalidOperationEx = new InvalidOperationException("Invalid Operation", ex);
                var message = string.Format("Divide failed - amount: {0}, by: {1}", amount, by);
                throw new ApplicationException(message, invalidOperationEx);
            }
        }
    }
}