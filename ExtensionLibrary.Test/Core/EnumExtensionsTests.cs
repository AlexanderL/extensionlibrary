﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ExtensionLibrary.Test.Core
{
    [TestClass]
    public class EnumExtensionsTests
    {
        [TestMethod]
        public void GetName()
        {
            var pdf = Formats.PDF;
            //Assert.AreEqual("PDF", pdf.ToString());
            //Assert.AreEqual("PDF", Enum.GetName(typeof(Formats), pdf));
            Assert.AreEqual("PDF", pdf.GetName());
        }
    }

    public enum Formats
    {
        PDF,
        DOCX,
        TXT
    }
}