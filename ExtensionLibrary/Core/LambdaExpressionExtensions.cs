﻿using System.Reflection;

namespace System.Linq.Expressions
{
    public static class LambdaExpressionExtensions
    {
        public static PropertyInfo ToPropertyInfo(this LambdaExpression expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            //TODO Need some additional check
            return memberExpression.Member as PropertyInfo;
        }
    }
}